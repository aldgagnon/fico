# FICO [![NPM version][npm-image]][npm-url] [![pipeline status](https://gitlab.com/aldgagnon/fico/badges/master/pipeline.svg)](https://gitlab.com/aldgagnon/fico/commits/main)
> First-in Chaos-out (FICO) is a proposed novell method of organizing data structures. In a FICO data structure, the oldest entry is at the head of the queue and requests to process items results in a random function being called on the data structure.

## Installation

```sh
$ npm install --save fico
```

## Usage

```js
const fico = require('fico');

fico.push({ name: 'red'});
fico.push({ name: 'green'});
fico.push({ name: 'blue'});

fico.pop();
//=> Item 'green' has been deleted

```
## License

Apache-2.0 © [Andrew Gagnon]()


[npm-image]: https://badge.fury.io/js/fico.svg
[npm-url]: https://npmjs.org/package/fico
[travis-image]: https://travis-ci.org/aldgagnon/fico.svg?branch=master
[travis-url]: https://travis-ci.org/aldgagnon/fico
[daviddm-image]: https://david-dm.org/aldgagnon/fico?theme=shields.io
[daviddm-url]: https://david-dm.org/aldgagnon/fico
