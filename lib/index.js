'use strict';

// Defines the Fico class
class Fico {
  // Constructor - initializes the array
  constructor() {
    this.arr = [];
  }

  // Deletes a random item from the array
  deleteRandomItem(arr) {
    let index = Math.floor(Math.random() * arr.length);
    arr.splice(index, 1);
    return null;
  }

  // Returns a random item from the array
  returnRandomItem(arr) {
    let index = Math.floor(Math.random() * arr.length);
    return arr[index];
  }

  // Push function
  push(obj) {
    this.arr.push(obj);
  }

  // Pop function
  pop() {
    const fnArray = [this.deleteRandomItem, this.returnRandomItem];

    let i = Math.floor(Math.random() * fnArray.length);
    return fnArray[i](this.arr);
  }
}

module.exports = Fico;
